import java.util.Optional;
import java.util.Scanner;


public class Main {
    public static void main (String[] args) {
        final TicTacToe game = new TicTacToe();
        final Scanner scanner = new Scanner(System.in);

        while (game.status() == Status.Active) {
            System.out.print("Player " + game.currentPlayer() + " move. Input cell (0 to 8): ");
            final int cellToMove = scanner.nextInt();

            if (!game.makeMove(cellToMove))
                System.out.println("Wrong cell");

            else
                for (int cell = 0; cell < 9; cell ++) {
                    final Optional<Player> figure = game.figureAt(cell);

                    if (figure.isEmpty())
                        System.out.print(".");
                    else if (figure.get() == Player.Cross)
                        System.out.print("X");
                    else if (figure.get() == Player.Circle)
                        System.out.print("O");

                    if ((cell + 1) % 3 == 0)
                        System.out.println();
                }
        }

        System.out.println(
            game.status() == Status.Draw ?
            "Draw" :
            "Player " + game.currentPlayer() + " won"
        );
    }
}
