import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

record StudentId (int groupId, int studentId) {}
record Student (StudentId id, String firstName, String lastName, int age) {}
record Grade (Student student, Integer grade) {}

public class Main {
    public static void main (String[] args) {
        Stream<Grade> studentsGrades = Stream.of(
                new Grade(new Student(new StudentId(213, 0), "Anna", "Ivanova", 19), 5),
                new Grade(new Student(new StudentId(214, 0), "Vasiliy", "Petrov", 21), 5),
                new Grade(new Student(new StudentId(213, 1), "Vladilen", "Sidorov", 20), 3),
                new Grade(new Student(new StudentId(214, 1), "Vladimir", "Ivanov", 22), 4),
                new Grade(new Student(new StudentId(213, 2), "Ivan", "Ivanov", 19), 5),
                new Grade(new Student(new StudentId(213, 3), "Petr", "Petrovich", 20), 3)
        );

        final String str = studentsGrades.collect(Collectors.teeing(
                Collectors.mapping(
                        grade -> grade.student().firstName() + " " + grade.student().lastName(),
                        Collectors.joining(", ", "[", "]")
                ),
                Collectors.averagingInt(Grade::grade),
                (list, averageGrade) -> String.format("average grade of %s is %f", list, averageGrade)
        ));

        System.out.println(str);
    }
}
