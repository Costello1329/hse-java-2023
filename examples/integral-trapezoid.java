import java.util.function.DoubleUnaryOperator;

public class Main {
    public static void main (String[] args) {
        System.out.println(integral(x -> x).applyAsDouble(5.));
        System.out.println(integral(Math::exp).applyAsDouble(1.));
        System.out.println(integral(x -> x * x).applyAsDouble(3.));
    }

    public static double integral (
        final DoubleUnaryOperator function,
        final double initialLeft,
        final double initialRight
    ) {
        final boolean inverse = initialLeft > initialRight;
        final double left = Math.min(initialLeft, initialRight);
        final double right = Math.max(initialLeft, initialRight);

        int segments = 1_000_000;
        final double h = (right - left) / segments;
        double sum = 0.;

        for (int i = 0; i < segments; i ++) {
            sum += function.applyAsDouble(left + i * h);
            sum += function.applyAsDouble(left + (i + 1) * h);
        }

        return (inverse ? -1. : 1.) * sum * h / 2.;
    }

    public static DoubleUnaryOperator integral (final DoubleUnaryOperator function) {
        return x -> integral(function, 0., x);
    }
}
