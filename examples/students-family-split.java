import java.util.stream.Collectors;
import java.util.stream.Stream;

record StudentId (int groupId, int studentId) {}
record Student (StudentId id, String firstName, String lastName, int age) {}

public class Main {
    public static void main (String[] args) {
        Stream.of(
            new Student(new StudentId(213, 0), "Anna", "Ivanova", 19),
            new Student(new StudentId(214, 0), "Vasiliy", "Petrov", 21),
            new Student(new StudentId(213, 1), "Vladilen", "Sidorov", 20),
            new Student(new StudentId(214, 1), "Vladimir", "Ivanov", 22),
            new Student(new StudentId(213, 2), "Ivan", "Ivanov", 19),
            new Student(new StudentId(213, 3), "Petr", "Petrovich", 20)
        ).collect(Collectors.partitioningBy(
            student -> student.lastName().startsWith("Ivanov"),
            Collectors.mapping(
                student -> student.firstName() + " " + student.lastName(),
                Collectors.joining(", ", "[", "]")
            )
        )).forEach(
            (isIvanov, studentsList) ->
                System.out.printf("%s: %s\n", isIvanov ? "Ivanovs" : "Non-Ivanovs", studentsList)
        );
    }
}
