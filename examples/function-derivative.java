import java.util.function.DoubleUnaryOperator;

public class Main {
    public static void main (String[] args) {
        System.out.println(derivative(x -> x).applyAsDouble(5.));
        System.out.println(derivative(Math::exp).applyAsDouble(1.));
        System.out.println(derivative(x -> x * x).applyAsDouble(3.));
    }

    public static DoubleUnaryOperator derivative (final DoubleUnaryOperator function) {
        return x -> {
            double[] approximations = new double[2];

            for (int i = 0; i <= 1 || Math.abs(approximations[0] - approximations[1]) > 1e-6; i ++) {
                final double h = Math.pow(2., - i);
                approximations[0] = approximations[1];
                approximations[1] = (function.applyAsDouble(x + h) - function.applyAsDouble(x - h)) / (2. * h);
            }

            return approximations[1];
        };
    }
}
