public class Coordinates {
    public Coordinates (final double x, final double y) {
        _x = x;
        _y = y;
    }

    public double get_x () { return _x; }
    public double get_y () { return _y; }

    Coordinates add (final Coordinates other) { return new Coordinates(_x + other._x, _y + other._y); }
    Coordinates subtract (final Coordinates other) { return new Coordinates(_x - other._x, _y - other._y); }

    final double _x;
    final double _y;
}
