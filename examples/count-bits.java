import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        final int number = scanner.nextInt();
        int count = 0;

        for (int i = 31; i >= 0; i --)
            count += (number >>> i) & 1;

        System.out.print("Количество единичных битов числа ");
        System.out.print(number);
        System.out.print(" (нашли мы): ");
        System.out.println(count);

        System.out.print("Количество единичных битов числа ");
        System.out.print(number);
        System.out.print(" (проверили через класс Integer): ");
        System.out.println(Integer.bitCount(number));
    }
}
