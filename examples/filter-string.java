import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final String string = scanner.next();
        System.out.println(filter(string));
    }

    public static String filter (String text) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < text.length(); i ++)
            if (
                (text.charAt(i) >= 'a' && text.charAt(i) <= 'z') ||
                (text.charAt(i) >= 'A' && text.charAt(i) <= 'Z') ||
                (text.charAt(i) >= '0' && text.charAt(i) <= '9')
            )
                builder.append(text.charAt(i));

        return builder.toString();
    }
}
