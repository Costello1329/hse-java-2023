import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число: ");
        final int number = scanner.nextInt();

        System.out.print("Все биты числа ");
        System.out.print(number);
        System.out.println(":");

        for (int i = 31; i >= 0; i --)
            System.out.print((number >>> i) & 1);
    }
}
