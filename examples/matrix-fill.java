import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        float k = (float) (n - 1) / 2;

        for (int i = 0; i < n; i ++) {
            for (int j = 0; j < n; j ++) {
                float deltaI = Math.abs(k - i);
                float deltaJ = Math.abs(k - j);
                System.out.print((int)(k - Math.max(deltaI, deltaJ)));
                System.out.print(" ");
            }

            System.out.println();
        }
    }
}
