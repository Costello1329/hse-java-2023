import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


record StudentId (int groupId, int studentId) {}
record Student (StudentId id, String firstName, String lastName, int age) {}

public class Main {
    public static void main (String[] args) {
        Map<String, Integer> studentsAge = Stream.of(
            new Student(new StudentId(213, 0), "Anna", "Ivanova", 19),
            new Student(new StudentId(214, 0), "Vasiliy", "Petrov", 21),
            new Student(new StudentId(213, 1), "Vladilen", "Sidorov", 20),
            new Student(new StudentId(214, 1), "Vladimir", "Ivanov", 22),
            new Student(new StudentId(213, 2), "Ivan", "Ivanov", 19),
            new Student(new StudentId(213, 3), "Petr", "Petrovich", 20)
        ).collect(Collectors.toMap(
            student -> String.join(" ", student.firstName(), student.lastName()),
            Student::age
        ));

        System.out.println(studentsAge);
    }
}
