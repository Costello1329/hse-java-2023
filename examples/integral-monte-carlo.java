import java.util.SplittableRandom;
import java.util.function.DoubleUnaryOperator;

public class Main {
    public static void main (String[] args) {
        final DoubleUnaryOperator linear = x -> 2 * x;
        final DoubleUnaryOperator square = integral(linear, 5.);
        System.out.println(square.applyAsDouble(1.));
        System.out.println(square.applyAsDouble(-2.));
        System.out.println(square.applyAsDouble(3.)); // Discrepancy due to the insufficient treshold
        System.out.println(square.applyAsDouble(4.)); // Discrepancy due to the insufficient treshold
    }

    public static double integral (
        final DoubleUnaryOperator function,
        final double initialLeft,
        final double initialRight,
        final double treshold
    ) {
        final boolean inverse = initialLeft >= initialRight;
        final double left = Math.min(initialLeft, initialRight);
        final double right = Math.max(initialLeft, initialRight);

        final SplittableRandom random = new SplittableRandom();
        final int points = 1_000_000;
        int positivePoints = 0;
        int negativePoints = 0;

        for (int i = 0; i < points; i ++) {
            final double pointX = random.nextDouble(left, right);
            final double pointY = random.nextDouble(- treshold, treshold);
            final double value = function.applyAsDouble(pointX);

            if (pointY > 0. && pointY < value)
                ++ positivePoints;

            else if (pointY < 0. && pointY > value)
                ++ negativePoints;
        }

        return (inverse ? -1. : 1.) * (positivePoints - negativePoints) / points * 2. * treshold * (right - left);
    }

    public static DoubleUnaryOperator integral (final DoubleUnaryOperator function, final double treshold) {
        return x -> integral(function, 0, x, treshold);
    }
}
