import java.util.function.DoubleUnaryOperator;

public class Main {
    public static void main (String[] args) {
        /// Expression: ln(x + e^x) / (x * sin(x) - cos(x))
        final DoubleUnaryOperator expression =
            divide(add(x -> x, Math::exp).andThen(Math::log), subtract(multiply(x -> x, Math::sin), Math::cos));

        System.out.println(expression.applyAsDouble(3.));
    }

    public static DoubleUnaryOperator add (final DoubleUnaryOperator first, final DoubleUnaryOperator second) {
        return (final double x) -> first.applyAsDouble(x) + second.applyAsDouble(x);
    }

    public static DoubleUnaryOperator subtract (final DoubleUnaryOperator first, final DoubleUnaryOperator second) {
        return (final double x) -> first.applyAsDouble(x) - second.applyAsDouble(x);
    }

    public static DoubleUnaryOperator multiply (final DoubleUnaryOperator first, final DoubleUnaryOperator second) {
        return (final double x) -> first.applyAsDouble(x) * second.applyAsDouble(x);
    }

    public static DoubleUnaryOperator divide (final DoubleUnaryOperator first, final DoubleUnaryOperator second) {
        return (final double x) -> first.applyAsDouble(x) / second.applyAsDouble(x);
    }
}
