package tafl;

import core.AbstractFigure;
import core.Player;

public class TaflKing extends TaflFigure {
    public TaflKing() { super(Player.WHITE); }

    @Override
    public String getPresentation() { return "◎"; }
}
