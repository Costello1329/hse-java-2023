package tafl.engines.brandubh;

import core.FieldFactory;
import core.Player;
import core.Vector;
import tafl.TaflField;
import tafl.TaflFigure;
import tafl.TaflKing;
import tafl.TaflWarrior;

public class BrandubhFieldFactory implements FieldFactory<TaflFigure, TaflField> {
    @Override
    public TaflField createField() {
        final TaflField field = new TaflField(7);

        field.placeFigure(field.getCenter(), new TaflKing());

        for (final Vector direction : Vector.DIRECTIONS_L_1) {
            field.placeFigure(field.getCenter().add(direction), new TaflWarrior(Player.WHITE));
            field.placeFigure(field.getCenter().add(direction.multiply(2)), new TaflWarrior(Player.BLACK));
            field.placeFigure(field.getCenter().add(direction.multiply(3)), new TaflWarrior(Player.BLACK));
        }

        return field;
    }
}
