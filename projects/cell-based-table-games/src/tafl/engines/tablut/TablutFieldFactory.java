package tafl.engines.tablut;

import core.FieldFactory;
import core.Player;
import core.Vector;
import tafl.TaflField;
import tafl.TaflFigure;
import tafl.TaflKing;
import tafl.TaflWarrior;

public class TablutFieldFactory implements FieldFactory<TaflFigure, TaflField> {
    @Override
    public TaflField createField() {
        final TaflField field = new TaflField(9);

        field.placeFigure(field.getCenter(), new TaflKing());

        for (final Vector direction : Vector.DIRECTIONS_L_1) {
            field.placeFigure(field.getCenter().add(direction), new TaflWarrior(Player.WHITE));
            field.placeFigure(field.getCenter().add(direction.multiply(2)), new TaflWarrior(Player.WHITE));

            final Vector base = field.getCenter().add(direction.multiply(4));
            field.placeFigure(base, new TaflWarrior(Player.BLACK));

            for (final Vector shift : Vector.DIRECTIONS_L_1) {
                final Vector candidate = base.add(shift);

                if (field.isCellValid(candidate)) {
                    field.placeFigure(candidate, new TaflWarrior(Player.BLACK));
                }
            }
        }

        return field;
    }
}
