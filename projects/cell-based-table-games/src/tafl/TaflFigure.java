package tafl;

import core.AbstractFigure;
import core.Player;

public abstract class TaflFigure extends AbstractFigure {
    public TaflFigure(final Player player) { super(player); }
}
