package tafl;

import core.*;

import java.util.Optional;

public class TaflEngine extends AbstractEngine<TaflFigure, TaflField, TaflMove> {
    protected TaflEngine(final FieldFactory<TaflFigure, TaflField> factory, final TaflSettings settings) {
        super(factory, Player.BLACK);
        this.settings = settings;
    }

    @Override
    protected void performMove(final TaflMove move) throws InvalidMoveException {
        final TaflFigure figure = validateMoveAndGetFigure(move);
        field.placeFigure(move.to(), figure);
        field.removeFigure(move.from());

        // check captures
        for (final Vector direction : Vector.DIRECTIONS_L_1) {
            final Vector candidate = move.to().add(direction);
            final Vector supporter = move.to().add(direction.multiply(2));

            if (isWarriorCaptured(candidate, supporter)) {
                field.removeFigure(candidate);
            }

            else if (isKingCaptured(candidate)) {
                setStatus(Status.WIN);
                return;
            }
        }

        if (
            // win by moving the king to the corner:
            field.isCornerCell(move.to()) && figure instanceof TaflKing ||
            // or win by stalemating the enemy:
            enemyHasNoMoves(getCurrentPlayer())
        ) {
            setStatus(Status.WIN);
        }
    }

    private boolean enemyHasNoMoves(final Player player) {
        for (int y = 0; y < field.getSize(); ++ y) {
            for (int x = 0; x < field.getSize(); ++ x) {
                final Vector position = new Vector(x, y);
                final Optional<TaflFigure> cell = field.getCell(position);

                if (cell.isEmpty() || cell.get().getPlayer() == player) {
                    continue;
                }

                final TaflFigure figure = cell.get();

                // check that the figure has at least one possible move
                for (final Vector direction : Vector.DIRECTIONS_L_1) {
                    final Vector candidate = position.add(direction);

                    if (
                        field.isCellValid(candidate) &&
                        field.getCell(candidate).isEmpty() &&
                        isCellAvailableForTheFigure(candidate, figure)
                    ) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private boolean isWarriorCaptured(final Vector candidate, final Vector supporter) {
        return
            field.isCellValid(candidate) &&
            field.getCell(candidate).isPresent() &&
            field.getCell(candidate).get() instanceof final TaflWarrior warrior &&
            warrior.getPlayer() != getCurrentPlayer() &&
            field.isCellValid(supporter) && (
                field.isCornerCell(supporter) ||
                field.getCell(supporter).isPresent() && field.getCell(supporter).get().getPlayer() == getCurrentPlayer()
            );
    }

    private boolean isKingCaptured(final Vector candidate) {
        if (
            !field.isCellValid(candidate) ||
            field.getCell(candidate).isEmpty() ||
            !(field.getCell(candidate).get() instanceof final TaflKing king) ||
            king.getPlayer() == getCurrentPlayer()
        ) {
            return false;
        }

        for (final Vector direction : Vector.DIRECTIONS_L_1) {
            final Vector supporter = candidate.add(direction);

            if (
                field.isCellValid(supporter) &&
                !field.isCornerCell(supporter) &&
                !field.isCenterCell(supporter) &&
                (field.getCell(supporter).isEmpty() || field.getCell(supporter).get().getPlayer() != getCurrentPlayer())
            ) {
                return false;
            }
        }

        return true;
    }

    private boolean isCellAvailableForTheFigure(final Vector cell, final TaflFigure figure) {
        return
            figure instanceof TaflWarrior ?
            !field.isCenterCell(cell) && !field.isCornerCell(cell) :
            !field.isCenterCell(cell) || settings.isKingAllowedToReturnToTheThrone();
    }

    private TaflFigure validateMoveAndGetFigure(final TaflMove move) throws InvalidMoveException {
        if (!field.isCellValid(move.from())) {
            throw new InvalidMoveException("initial cell is invalid");
        }

        if (field.getCell(move.from()).isEmpty()) {
            throw new InvalidMoveException("initial cell is empty");
        }

        if (field.getCell(move.from()).get().getPlayer() != getCurrentPlayer()) {
            throw new InvalidMoveException("figure in the initial cell is not owned by the current player");
        }

        if (!field.isCellValid(move.to())) {
            throw new InvalidMoveException("target cell is invalid");
        }

        if (field.getCell(move.to()).isPresent()) {
            throw new InvalidMoveException("target cell is already occupied");
        }

        if (move.from().x() != move.to().x() && move.from().y() != move.to().y()) {
            throw new InvalidMoveException("move is not vertical nor horizontal");
        }

        final int moveDistance = Vector.distanceL1(move.from(), move.to());

        if (moveDistance == 0) {
            throw new InvalidMoveException("initial and target cells should be different");
        }

        if (settings.isFigureMoveDistanceRestricted() && moveDistance > 1) {
            throw new InvalidMoveException("move distance should be equal to one, because it's restricted");
        }

        final TaflFigure figure = field.getCell(move.from()).get();

        if (!isCellAvailableForTheFigure(move.to(), figure)) {
            throw new InvalidMoveException("target cell is restricted to visit by this figure");
        }

        final Vector direction = move.to().subtract(move.from()).divide(moveDistance);

        for (
            Vector current = move.from().add(direction);
            !current.equals(move.to());
            current = current.add(direction)
        ) {
            if (field.getCell(current).isPresent()) {
                throw new InvalidMoveException(
                    "there should be no other figures on the path from the initial cell to the target cell, but " +
                    "the cell " + current + " is already occupied by other figure"
                );
            }

            if (!isCellAvailableForTheFigure(current, figure)) {
                throw new InvalidMoveException(
                    "all cells on the path from the initial cell to the target cell must be available to the current " +
                    "figure, but the cell " + current + " – isn't"
                );
            }
        }

        return figure;
    }

    private final TaflSettings settings;
}
