package tafl;

import core.AbstractField;
import core.Vector;

public class TaflField extends AbstractField<TaflFigure> {
    public TaflField(final int size) {
        super(size);
        this.center = new Vector(size / 2, size / 2);
    }

    @Override
    public String getCellPresentation(final Vector cell) {
        return isCenterCell(cell) || isCornerCell(cell) ? "▪" : "▫";
    }

    public boolean isCenterCell(final Vector cell) { return center.equals(cell); }
    public boolean isCornerCell(final Vector cell) { return Vector.distanceL1(center, cell) == getSize() - 1; }

    public Vector getCenter() { return center; }

    private final Vector center;
}
