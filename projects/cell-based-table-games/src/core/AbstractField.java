package core;

import java.util.ArrayList;
import java.util.Optional;

public abstract class AbstractField<GameFigure extends AbstractFigure> {
    public AbstractField(final int size) {
        this.size = size;
        field = new ArrayList<>();

        for (int i = 0; i < size * size; ++ i) {
            field.add(Optional.empty());
        }
    }

    public String getPresentation() {
        StringBuilder builder = new StringBuilder();

        for (int y = size - 1; y >= 0; -- y) {
            for (int x = 0; x < size; ++ x) {
                final Vector position = new Vector(x, y);
                final Optional<GameFigure> cell = getCell(position);

                builder.append(
                    cell.isPresent() ?
                    cell.get().getPresentation() :
                    getCellPresentation(position)
                );
                builder.append(x == size - 1 ? "\n" : " ");
            }
        }

        return builder.toString();
    }

    protected abstract String getCellPresentation(final Vector cell);

    public boolean isCellValid(final Vector cell) {
        return cell.x() >= 0 && cell.x() < size && cell.y() >= 0 && cell.y() < size;
    }

    public void placeFigure(final Vector cell, final GameFigure figure) {
        field.set(cell.y() * size + cell.x(), Optional.of(figure));
    }

    public Optional<GameFigure> getCell(final Vector cell) { return field.get(cell.y() * size + cell.x()); }
    public void removeFigure(final Vector cell) { field.set(cell.y() * size + cell.x(), Optional.empty()); }

    public int getSize() { return size; }

    private final int size;
    private final ArrayList<Optional<GameFigure>> field;
}
