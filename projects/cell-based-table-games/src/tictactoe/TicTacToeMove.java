package tictactoe;

import core.Move;
import core.Vector;

public record TicTacToeMove(Vector cell) implements Move {}
