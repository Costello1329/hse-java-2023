package tictactoe;

import core.*;

import java.util.ArrayList;
import java.util.Optional;

public class TicTacToeEngine extends AbstractEngine<TicTacToeFigure, TicTacToeField, TicTacToeMove> {
    public TicTacToeEngine(final int size) { super(new TicTacToeFieldFactory(size), Player.WHITE); }

    @Override
    public void performMove(final TicTacToeMove move) throws InvalidMoveException {
        if (!field.isCellValid(move.cell())) {
            throw new InvalidMoveException("target cell doesn't exist");
        }

        if (field.getCell(move.cell()).isPresent()) {
            throw new InvalidMoveException("target cell should be empty");
        }

        field.placeFigure(move.cell(), new TicTacToeFigure(getCurrentPlayer()));

        if (checkWin(move.cell()))
            setStatus(Status.WIN);

        else if (checkDraw())
            setStatus(Status.DRAW);
    }

    private boolean checkWin(final Vector cell) {
        final int n = field.getSize();

        final ArrayList<Vector> lineStarts = new ArrayList<>();
        final ArrayList<Vector> lineDirections = new ArrayList<>();

        lineStarts.add(new Vector(0, cell.y()));
        lineDirections.add(new Vector(1, 0));
        lineStarts.add(new Vector(cell.x(), 0));
        lineDirections.add(new Vector(0, 1));

        /// cell is placed on the diagonal
        if (cell.x() == cell.y() || cell.x() + cell.y() == n - 1) {
            lineStarts.add(new Vector(0, 0));
            lineDirections.add(new Vector(1, 1));
            lineStarts.add(new Vector(0, n - 1));
            lineDirections.add(new Vector(1, -1));
        }

        for (int line = 0; line < lineStarts.size(); ++ line) {
            if (checkLine(lineStarts.get(line), lineDirections.get(line))) {
                return true;
            }
        }

        return false;
    }

    private boolean checkLine(final Vector start, final Vector direction) {
        for (int k = 0; k < field.getSize(); ++ k) {
            final Vector currentCell = start.add(direction.multiply(k));
            final Optional<TicTacToeFigure> figure = field.getCell(currentCell);

            if (figure.isEmpty() || figure.get().getPlayer() != getCurrentPlayer()) {
                return false;
            }
        }

        return true;
    }

    private boolean checkDraw() {
        for (int x = 0; x < field.getSize(); ++ x) {
            for (int y = 0; y < field.getSize(); ++ y) {
                if (field.getCell(new Vector(x, y)).isEmpty()) {
                    return false;
                }
            }
        }

        return true;
    }
}
